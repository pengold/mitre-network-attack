from __future__ import annotations

import unittest
from .subnet import *


class Host:
    
    def __init__(self, code, subnet=None, sensitive=False, attacker=False, techniques=[], required=[]):
        self.code = code
        self.subnet = subnet
        self.sensitive = sensitive
        self.techniques = techniques # 2D list
        self.required = required # Required techniques to compromise a host
        self.attacker = attacker
        self.suspection = 0
        
        self.avail_techniques = list()
        for sequence in techniques:
            self.avail_techniques.extend(sequence)
        self.avail_techniques = list(set(self.avail_techniques))
        
        self.performed_techniques = list()
        self.technique_tracks = [0 for i in range(len(self.techniques))]
        self.technique_len = [len(i) for i in self.techniques] # Length of each technique sequence
        self.compromised = False
        
        self.history = list()

    def __str__(self):
        return f'Host {self.code}'

    def __repr__(self):
        return f'Host {self.code}'

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.code == other.code
        else:
            return False
    
    def get_info(self) -> str:
        return f'-- Host {self.code} - Sensitive: {self.sensitive} - Required: {self.required} - Compromised: {self.compromised} - Suspection: {self.suspection}'
        
    def from_data(data: dict) -> Host:
        sensitive = False
        if 'sensitive' in data: 
            sensitive = data['sensitive']
        
        attacker = False
        if 'attacker' in data:
            attacker = data['attacker']
            
        techniques = []
        if 'techniques' in data: 
            techniques = data['techniques']
        
        required = []
        if 'required' in data:
            required = data['required']
        
        return Host(code=data['code'],
                    sensitive=sensitive, 
                    attacker=attacker,
                    techniques=techniques, 
                    required=required)

    def add_suspection(self, amount:float) -> Host:
        self.suspection += amount
        return self
        
    def perform_technique(self, technique: str) -> bool:
        visible_techniques = self.get_visible_techniques()
        # In case same technique appears in different sequence
        indexes = [i for i, item in enumerate(visible_techniques) if item == technique]
        if(len(indexes) == 0):
            # If technique not in visible technique list
            self.history.append(['perform', technique, False])
            return False
        
        for index in indexes:
            self.technique_tracks[index] += 1
        self.performed_techniques.append(technique)
        self.history.append(['perform', technique, True])
        
        # Check if finish required techniques
        if not self.compromised:
            visible_techniques = self.get_visible_techniques()
            indexes = [i for i, item in enumerate(visible_techniques) if item == 'done']
            for index in indexes:
                if index in self.required:
                    self.compromised = True
                    self.history.append(['compromised', True])
                    break
        return True
    
    def get_visible_techniques(self) -> list:
        visible_techniques = list()
        for index in range(len(self.techniques)):
            track = self.technique_tracks[index]
            if track < len(self.techniques[index]):
                visible_techniques.append(self.techniques[index][track])
            else:
                visible_techniques.append('done')
        return visible_techniques
    
    def protected_by_defender(self):
        # Not implemented yet
        # print('Protect success ', self)
        self.suspection = 0

class TestingHost(unittest.TestCase):
    
    def test_perform_technique(self):
        h = Host('code', techniques=[['T1566', 'T1204'], ['T1199', 'T1205']])
        self.assertEqual(h.get_visible_techniques(), ['T1566', 'T1199'])
        self.assertEqual(h.perform_technique('T1204'), False)
        self.assertEqual(h.perform_technique('T1205'), False)
        self.assertEqual(h.perform_technique('T1566'), True)
        self.assertEqual(h.get_visible_techniques(), ['T1204', 'T1199'])
        self.assertEqual(h.perform_technique('T1566'), False)
        self.assertEqual(h.perform_technique('T1199'), True)
        self.assertEqual(h.get_visible_techniques(), ['T1204', 'T1205'])
        self.assertEqual(h.perform_technique('T1205'), True)
        self.assertEqual(h.get_visible_techniques(), ['T1204', 'done'])
        self.assertEqual(h.perform_technique('T1204'), True)
        self.assertEqual(h.get_visible_techniques(), ['done', 'done'])
        
    # def test_get_state_vector(self):
    #     h = Host('code', techniques=[['T1566', 'T1204'], ['T1199', 'T1078']], required=[0])
    #     avail_techniques = ['T1566', 'T0000', 'T1199', 'T1078', 'T1204']
    #     self.assertEqual(h.get_state_vector(avail_techniques), [0, 1, 0, 1, 0, 0])
    #     h.perform_technique('T1566')
    #     self.assertEqual(h.get_state_vector(avail_techniques), [0, 2, 0, 1, 0, 1])
    #     h.perform_technique('T1199')
    #     self.assertEqual(h.get_state_vector(avail_techniques), [0, 2, 0, 2, 1, 1])
    #     h.perform_technique('T1078')
    #     self.assertEqual(h.get_state_vector(avail_techniques), [0, 2, 0, 2, 2, 1])
    #     h.perform_technique('T1204')
    #     self.assertEqual(h.get_state_vector(avail_techniques), [1, 2, 0, 2, 2, 2])
        
if __name__ == '__main__':
    # python3 -m unittest src.component.host
    import json
    json_file = open('data/test_scenario.json')
    data = json.load(json_file)
    h = Host.from_data(data['hosts'][1])
    # h.techniques = [['T1566', 'T1204'], ['T1199', 'T1204']]
    print(h.techniques)
    # print(h.required)
    # print(h.technique_len)
    # print(h.get_state_vector(['T1566', 'T0000', 'T1199', 'T1078', 'T1204']))
    