from src.environment.environment import MitreEnv, MitreEnvVector
from .scenario_reader import *


def run_environment():
    reader = ScenarioReader()
    technique_info = reader.read_technique_info("data/technique_info.csv")
    pprint(technique_info)
    
    json_file = open('data/test_scenario.json')
    data = json.load(json_file)
    net = Network()
    net.initialize(data).set_foothold('External', 'Attacker')
    env = MitreEnv(net, technique_info)
    for idx, action in enumerate(env.action_space.space):
        print(idx, action)
    print(env.reset())
    print(env.step(36)) # Scan Subnet S0
    env.render()
    print(env.step(10)) # Apply M00 T1566
    env.render()
    print(env.step(6)) # Apply M00 T1204
    env.render()
    # print(env.step(73)) # Scan Subnet S1
    # env.render()
    # print(env.step(72)) # Apply M12 T1570
    # env.render()
    # print(env.step(64)) # Apply M12 T1110
    # env.render()
    # print(env.step(63)) # Apply M12 T1078
    # env.render()
    print(env.step(98)) # Scan Subnet S2
    env.render()
    print(env.step(90)) # Apply M21 T1133
    env.render()
    print(env.step(95)) # Apply M21 T1542
    env.render()
    print(env.step(93)) # Apply M21 T1529
    env.render()
    
def run_defender():
    from .defender import DefenderAgent
    reader = ScenarioReader()
    mitigation_info = reader.read_mitigation_info("data/mitigation_info.csv")
    technique_info = reader.read_technique_info("data/technique_info.csv")
    pprint(mitigation_info)
    
    json_file = open('data/test_scenario.json')
    data = json.load(json_file)
    net = Network()
    net.initialize(data).set_foothold('External', 'Attacker')
    defender = DefenderAgent(net, mitigation_info)
    env = MitreEnv(net, technique_info, defender)
    # pprint(env.action_space.space)
    env.reset()
    print(env.step(13))
    env.render()
    print(env.step(24))
    env.render()
    print(env.step(20))
    env.render()
    print(env.step(87))
    env.render()
    print(env.step(104))
    env.render()
    print(env.step(109))
    env.render()
    print(env.step(107))

def run_environment_vector():
    reader = ScenarioReader()
    
    json_file = open('mitre/scenario/16_2_5.json')
    data = json.load(json_file)
    net = Network()
    net.initialize(data).set_foothold('External', 'Attacker')
    env = MitreEnvVector(net, 
                         technique_info=reader.read_technique_info("data/technique_info.csv"), 
                         tatic_emb_path='mitre/tatic_onehot.pickle', 
                         technique_emb_path='mitre/technique_weight.pickle', 
                         knn_rate=0.01)
    print(env.get_knn_actions(1))

if __name__ == '__main__':
    # run_environment()
    # run_defender()
    run_environment_vector()
    
    # reader = ScenarioReader()
    # mitigation_info = reader.read_mitigation_info("data/mitigation_info.csv")
    # technique_info = reader.read_technique_info("data/technique_info.csv")
    # json_file = open('data/test_scenario.json')
    # data = json.load(json_file)
    # net = Network()
    # net.initialize(data).set_foothold('External', 'Attacker')
    # env = MitreEnv(net, technique_info)
    # print(env.reset())