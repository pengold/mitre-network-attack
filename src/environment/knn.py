import json
import numpy as np
import faiss
import json
import random
import pickle
import pprint

from src.component.network import Network
from src.environment.action import ActionSpaceVector
from src.environment.node2vec import TopologyEmbedding
from sklearn.neighbors import KNeighborsClassifier


class FaissKNeighbors:
    def __init__(self, k=5):
        self.index = None
        self.y = None
        self.k = k

    def fit(self, X, y):
        X = np.array(X)
        self.index = faiss.IndexFlatL2(X.shape[1])
        self.index.add(X.astype(np.float32))
        self.y = y

    def kneighbors(self, X):
        X = np.array(X)
        distances, indices = self.index.search(X.astype(np.float32), k=self.k)
        # votes = self.y[indices]
        # predictions = np.array([np.argmax(np.bincount(x)) for x in votes])
        return distances, indices
    
if __name__ == '__main__':
    json_file = open('data/test_scenario.json')
    data = json.load(json_file)
    net = Network()
    net.initialize(data).set_foothold('External', 'Attacker')
    tatic_emb = pickle.load(open('mitre/tatic_onehot.pickle', 'rb'))
    technique_emb = pickle.load(open('mitre/technique_weight.pickle', 'rb'))
    topology_emb = TopologyEmbedding(dimensions=3).host_level(net)
    action_space = ActionSpaceVector(net, tatic_emb, topology_emb, technique_emb)
    print('ACTION SPACE', action_space.n)
    # print(action_space.space)

    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(action_space.space_vector, y=np.arange(action_space.n))
    neigh_dist, neigh_idx = knn.kneighbors([action_space.space_vector[1]])
    print(neigh_idx)
    
    fknn = FaissKNeighbors(k=5)
    fknn.fit(action_space.space_vector, y=np.arange(action_space.n))
    print(fknn.kneighbors([action_space.space_vector[1]]))