from __future__ import annotations
from . import *

class Subnet:

    def __init__(self, code: str):
        self.code = code
        self.hosts = dict()
        self.connections = []

        self.vector = []
        
        self.reachable = False
        self.scanned = False
        self.avail_techniques = list()

    def __str__(self):
        return f'Subnet {self.code}'

    def __repr__(self):
        return f'Subnet {self.code}'

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.code == other.code
        return False
    
    def set_foothold(self, host_code:str) -> Subnet:
        self.reachable = 1
        self.scanned = True
        for connection in self.connections:
            connection.des.reachable = True
            
        self.hosts[host_code].compromised = True

    def add_host(self, host):
        self.hosts[host.code] = host
        self.avail_techniques.extend(host.avail_techniques)
        self.avail_techniques = list(set(self.avail_techniques))

    def add_connection(self, connection):
        self.connections.append(connection)
    
    def perform_scan(self) -> bool:
        # Check if already scanned then return False
        if self.scanned:
            return False
        
        # Check if there are direct connect with scanned subnet
        checked = False 
        for connection in self.connections:
            if connection.des.scanned:
                checked = True
            break
        
        if checked:
            self.scanned = True
            for connection in self.connections:
                connection.des.reachable = True
            return True
        else:
            return False
        

if __name__ == '__main__':
    s = Subnet(code='S1')
    # print(s.get_state_vector())
    # s.scanned = True
    # print(s.get_state_vector())
    s2 = Subnet(code='S2')
    s2.add_host(Host('M1', techniques=[["T1566", "T1204"], ["T1199", "T1078"]]))
    s2.add_host(Host('M2', techniques=[["T1534", "T1204", "T1056", "T1078"]]))
    s3 = Subnet(code='S3')
    s4 = Subnet(code='S4')
    s.add_connection(Connection(s, s2))
    s2.add_connection(Connection(s2, s))
    s.reachable = True
    s.scanned = True
    s2.reachable = True
    s2.add_connection(Connection(s2, s3))
    s3.add_connection(Connection(s3, s2))
    s2.add_connection(Connection(s2, s4))
    s4.add_connection(Connection(s4, s2))
    print(s.get_state_vector(), s2.get_state_vector(), s3.get_state_vector(), s4.get_state_vector())
    s2.perform_scan()
    print(s.get_state_vector(), s2.get_state_vector(), s3.get_state_vector(), s4.get_state_vector())