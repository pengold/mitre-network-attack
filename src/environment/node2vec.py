
import matplotlib.pyplot as plt
import networkx as nx
from node2vec import Node2Vec
import numpy as np

from src.environment.scenario_reader import ScenarioReader

class TopologyEmbedding():

    def __init__(self, render=False, dimensions=3) -> None:
        self.render = render
        self.dimensions = dimensions

    def subnet_level(self, network):
        graph = nx.Graph()
        checked_subnets = []
        for subnet in network.subnets.values():
            for connection in subnet.connections:
                if connection.des.code not in checked_subnets:
                    graph.add_edge(subnet.code, connection.des.code)
            checked_subnets.append(subnet.code)
        if self.render:
            nx.draw(graph)
            plt.show()

        num_nodes = len(network.subnets)
        node2vec = Node2Vec(graph, dimensions=self.dimensions, walk_length=num_nodes * 2, num_walks=1000, workers=4)
        model = node2vec.fit(window=10, min_count=1, batch_words=4)

        for subnet in network.subnets:
            print(subnet, model.wv[subnet])

        if self.render:
            for subnet in network.subnets:
                e = model.wv[subnet]
                plt.scatter(e[0], e[1])
                plt.annotate(subnet, (e[0], e[1]))

            plt.show()
        
        return model

    def host_level(self, network):
        graph = nx.Graph()
        for subnet in network.subnets.values():
            host_key_list = list(subnet.hosts.keys())

            if len(host_key_list) == 1:
                graph.add_node(subnet.hosts[host_key_list[0]].code)
                continue

            for i in range(len(host_key_list)):
                for j in range(i + 1, len(host_key_list)):
                    graph.add_edge(subnet.hosts[host_key_list[i]].code, subnet.hosts[host_key_list[j]].code)

        checked_subnets = []
        for subnet in network.subnets.values():
            for connection in subnet.connections:
                if connection.des.code not in checked_subnets:

                    for host1 in subnet.hosts.values():
                        for host2 in network.subnets[connection.des.code].hosts.values():
                            graph.add_edge(host1.code, host2.code)
            checked_subnets.append(subnet.code)

        if self.render:
            # print(graph.number_of_edges())
            nx.draw(graph)
            plt.show()

        num_nodes = len(network.subnets)
        node2vec = Node2Vec(graph, dimensions=self.dimensions, walk_length=num_nodes * 2, num_walks=1000, workers=4)
        model = node2vec.fit(window=10, min_count=1, batch_words=4)

        results = dict()
        for subnet in network.subnets.values():
            subnet_emb = list()
            for host in subnet.hosts:
                # print(host, model.wv[host])
                results[host] = model.wv[host]
                subnet_emb.append(model.wv[host])
            # print(subnet, np.average(subnet_emb, axis=0))
            results[subnet.code] =  np.average(subnet_emb, axis=0)


        if self.render:
            for subnet in network.subnets.values():
                for host in subnet.hosts:
                    e = model.wv[host]
                    plt.scatter(e[0], e[1])
                    plt.annotate(host, (e[0], e[1]))
            # for key, e in results.items():
            #     plt.scatter(e[0], e[1])
            #     plt.annotate(key, (e[0], e[1]))

            plt.show()
        
        return results


if __name__ == '__main__':
    reader = ScenarioReader()
    network = reader.read_network("data/test_scenario.json")
    # print(network.subnets)
    node2vec = TopologyEmbedding(render=True, dimensions=3)
    node2vec.host_level(network)