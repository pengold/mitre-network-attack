from __future__ import annotations

class Technique:

    def __init__(self, code='', complexity=0, detection=0, timestep=1):
        self.code = code
        self.complexity = complexity
        self.detection = detection
        self.timestep = timestep

    def __repr__(self):
        return f'Technique {self.code} | {self.complexity} | {self.detection} | {self.timestep}'

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.code == other.code
        else:
            return False
        
    def from_data(data: list) -> list[Technique]:
        technique_ids = list
        for host in data['hosts']:
            if 'required_techniques' in host:
                technique_ids.extend(host['required_techniques'])
            if 'additional_techniques' in host:
                technique_ids.extend(host['additional_techniques'])
        technique_ids = list(set(technique_ids)) # Remove duplicate
        
        techniques = list()
        for technique_id in technique_ids:
            techniques.append(Technique(technique_id))
        return techniques