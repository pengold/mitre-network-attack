import pandas as pd
import argparse
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader, random_split

# Formular embedding_dimensions =  number_of_categories**0.25
no_techniques = len(pd.read_csv("data/attack.csv")) #566 => 5
no_platforms = len(pd.read_csv("data/platform.csv")) #12 => 2
no_permissions = len(pd.read_csv("data/permission.csv")) #5 => 1.5 
no_groups = len(pd.read_csv("data/group.csv")) #133 => 3.5
no_malwares = len(pd.read_csv("data/malware.csv")) #474 => 5
no_mitigations = len(pd.read_csv("data/mitigation.csv")) #43 => 2.5

class MitreDataset(Dataset):
    
    def __init__(self, positive_csv, negative_csv):
        pos_data =pd.read_csv(positive_csv)
        neg_data =pd.read_csv(negative_csv)
        data = pd.concat([pos_data, neg_data])
        x = data.loc[:, data.columns != 'output'].values
        y = data['output'].values
        self.x = torch.tensor(x, dtype=torch.long)
        self.y = torch.tensor(y, dtype=torch.long)
        
    def __len__(self):
        return len(self.y)
    
    def __getitem__(self, idx):
        return self.x[idx], self.y[idx]
        

class EmbeddingNetwork(nn.Module):

    def __init__(self, embedding_size=5):
        super().__init__()
        self.technique_embedding = nn.Embedding(num_embeddings=no_techniques, embedding_dim=embedding_size)
        
        self.platform_embedding = nn.Embedding(num_embeddings=no_platforms, embedding_dim=2)
        self.permission_embedding = nn.Embedding(num_embeddings=no_permissions, embedding_dim=2)
        self.group_embedding = nn.Embedding(num_embeddings=no_groups, embedding_dim=4)
        self.malware_embedding = nn.Embedding(num_embeddings=no_malwares, embedding_dim=5)
        self.mitigation_embedding = nn.Embedding(num_embeddings=no_mitigations, embedding_dim=3)
        
        self.fc = nn.Linear(16, embedding_size)
        
        self.cos = nn.CosineSimilarity(dim=1, eps=1e-6)

    def forward(self, x):
        # print(x)
        # print(x[:,1])
        technique_emb = self.technique_embedding(x[:,0])
        platform_emb = self.platform_embedding(x[:,1])
        permission_emb = self.permission_embedding(x[:,2])
        group_emb = self.group_embedding(x[:,3])
        malware_emb = self.malware_embedding(x[:,4])
        mitigation_emb = self.mitigation_embedding(x[:,5])
        
        feature_emb = torch.cat((platform_emb, permission_emb, group_emb, malware_emb, mitigation_emb), dim=1)
        feature_emb = self.fc(feature_emb)

        # Apply L2-normalize for both book embedding and link embedding
        technique_emb = F.normalize(technique_emb, dim=0, p=2)
        feature_emb = F.normalize(feature_emb, dim=0, p=2)

        return self.cos(technique_emb, feature_emb)


def train(epoches):
    data = MitreDataset('data/positive_samples.csv', 'data/negative_samples.csv')
    train_size = round(len(data) * 0.95)
    test_size = len(data) - train_size
    train_set, test_set = random_split(data, [train_size, test_size], generator=torch.Generator().manual_seed(42))

    train_loader = DataLoader(train_set, batch_size=10, shuffle=True)
    test_loader = DataLoader(test_set, batch_size=1, shuffle=True)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)
    model = EmbeddingNetwork(20)
    model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    loss_fn = nn.MSELoss()

    print(f'Start training {epoches} epoches')
    for epoch in range(epoches):
        epoch_loss = 0
        for x, y in train_loader:
            x, y = x.to(device), y.to(device)
            optimizer.zero_grad()
            output = model(x)
            loss = loss_fn(output.double(), y.double())
            loss.backward()
            epoch_loss += loss.item()
            optimizer.step()
        print(epoch, epoch_loss/len(train_loader))

    torch.save(model.state_dict(), "model_e20.pt")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--epoch", type=int, default=100)
    args = parser.parse_args()

    train(args.epoch)

