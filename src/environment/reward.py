from sre_constants import SUCCESS


class Reward():

    INIT = -5
    SCAN = 20
    TECHNIQUE = 5
    COMPROMISED = 10
    SENSITIVE = 100
    SUCCESS_DEFENDER = -50