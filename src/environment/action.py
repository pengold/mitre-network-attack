

import json
import random
import pickle
import pprint
import numpy as np

from src.component.network import Network
from src.environment.node2vec import TopologyEmbedding
from sklearn.neighbors import KNeighborsClassifier


class Action:
    
    TYPE_SCAN = 'scan_subnet'
    TYPE_TECH = 'apply_technique'
    
    def __init__(self, action_type, target, technique=None):
        self.type = action_type
        self.target = target
        self.technique = technique
    
    def __repr__(self):
        return '{:11} - {} - {:10}'.format(self.type, self.target, str(self.technique))
    
    def __str__(self):
        return '{:11} - {} - {:10}'.format(self.type, self.target, str(self.technique))
        
class ActionSpace:
    
    def __init__(self, network):
        self.space = list()
        for subnet in network.subnets.values():
            subnet_techniques = list() # Just to test to match with the ActionSpaceVector version
            for host in subnet.hosts.values():
                if host.attacker:
                    continue
                for technique in network.avail_techniques:
                    self.space.append(Action(Action.TYPE_TECH, host, technique))
                    subnet_techniques.append(technique)
            if len(subnet_techniques) == 0:
                continue
            self.space.append(Action(Action.TYPE_SCAN, subnet))
            
        self.n = len(self.space)
        
    def get_action(self, index):
        return self.space[index]
    
    def sample(self):
        index = random.randrange(self.n)
        return index

class ActionSpaceVector(ActionSpace):

    def __init__(self, network, tatic_emb, topology_emb, technique_emb):
        self.space = list()
        self.space_vector = list()

        self.topology_emb = topology_emb
        self.technique_emb = technique_emb
        self.tatic_emb = tatic_emb

        for subnet in network.subnets.values():
            subnet_techniques = list()
            for host in subnet.hosts.values():
                if host.attacker:
                    continue
                for technique in network.avail_techniques:
                    self.space.append(Action(Action.TYPE_TECH, host, technique))

                    action_vector = self._get_action_vector(host.code, [technique], Action.TYPE_TECH)
                    self.space_vector.append(action_vector)
                    
                    subnet_techniques.append(technique)

            if len(subnet_techniques) == 0:
                continue

            self.space.append(Action(Action.TYPE_SCAN, subnet))

            action_vector = self._get_action_vector(subnet.code, subnet_techniques, Action.TYPE_SCAN)
            self.space_vector.append(action_vector)

        self.n = len(self.space)

    def _get_action_vector(self, code, techniques, action_type=None):
        action_vector = list()
        # if action_type is not None:
        #     if action_type == Action.TYPE_SCAN:
        #         action_vector.extend([0])
        #     else:
        #         action_vector.extend([5])
        action_vector.extend(np.average([self.tatic_emb[x] for x in techniques], axis=0))
        action_vector.extend(10 * self.topology_emb[code])
        action_vector.extend(np.average([self.technique_emb[x] for x in techniques], axis=0))
        return action_vector

    # def set_vector(self, topology_emb, technique_emb):
    #     len_technique_emb = len(technique_emb['T1534'])
    #     self.space_vector = list()
    #     for action in self.space:
    #         action_vector = list()
    #         action_vector.extend(3 * topology_emb[action.target.code])
    #         if action.technique:
    #             action_vector.extend(technique_emb[action.technique])
    #         else:
    #             action_vector.extend([0] * len_technique_emb)
    #         self.space_vector.append(action_vector)
        
if __name__ == '__main__':
    json_file = open('mitre/scenario/16_2_5.json')
    data = json.load(json_file)
    net = Network()
    net.initialize(data).set_foothold('External', 'Attacker')
    tatic_emb = pickle.load(open('mitre/tatic_onehot.pickle', 'rb'))
    technique_emb = pickle.load(open('mitre/technique_weight.pickle', 'rb'))
    topology_emb = TopologyEmbedding(dimensions=3).host_level(net)
    action_space = ActionSpaceVector(net, tatic_emb, topology_emb, technique_emb)
    print('ACTION SPACE', action_space.n)
    # print(action_space.space)

    # knn = KNeighborsClassifier(n_neighbors=20)
    # knn.fit(action_space.space_vector, y=np.arange(action_space.n))
    # neigh_dist, neigh_idx = knn.kneighbors([action_space.space_vector[57]])
    # print(neigh_dist, neigh_idx)
    # for i, idx in enumerate(neigh_idx[0]):
    #     print(idx, action_space.space[idx], neigh_dist[0][i])

    for i in range(action_space.n):
        print(i, action_space.space[i], action_space.space_vector[i])