from __future__ import annotations

class Mitigation:

    def __init__(self, code='', complexity=0, timestep=1, techniques=[]):
        self.code = code
        self.complexity = complexity
        self.timestep = timestep
        self.techniques = techniques

    def __repr__(self):
        return f'Mitigation {self.code}'

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.code == other.code
        else:
            return False
        
    # def from_data(data: dict) -> list[Mitigation]:
        
    #     for host in data['hosts']:
            