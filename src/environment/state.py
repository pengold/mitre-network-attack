import json
from src.component.network import Network

class State:
        
    def get_observation(self, network):
        state = list()
        # print('VO KIA', network.subnets)
        for subnet in network.subnets.values():
            # print('VO DAY')
            # state.extend(['||'] + self.get_subnet_obs(subnet, network.avail_techniques))
            state.extend(self.get_subnet_obs(subnet, network.avail_techniques))
        return state
    
    def get_subnet_obs(self, subnet, avail_techniques):
        if avail_techniques is None:
            avail_techniques = subnet.avail_techniques
        
        state = list()
        state.append(1 if subnet.reachable else 0)
        
        if not subnet.scanned:
            state.extend([-1] * ((len(avail_techniques) + 2 ) * len(subnet.hosts)))
            return state
        
        for code, host in subnet.hosts.items():
            state.extend(self.get_host_obs(host, avail_techniques))
        return state
    
    def get_host_obs(self, host, avail_techniques):
        compromised = 1 if host.compromised else 0
        return [compromised, host.suspection] + self.get_technique_obs(host, avail_techniques)
    
    def get_technique_obs(self, host, avail_techniques):
        if avail_techniques == None:
            avail_techniques = host.avail_techniques
        vector = [0] * len(avail_techniques)
        for i, technique in enumerate(avail_techniques):
            if technique in host.performed_techniques:
                vector[i] = 2
            elif technique in host.get_visible_techniques():
                vector[i] = 1 
        return vector
    
if __name__ == '__main__':
    json_file = open('data/test_scenario.json')
    data = json.load(json_file)
    net = Network()
    net.initialize(data).set_foothold('External', 'Attacker')
    print(State().get_observation(net))
    