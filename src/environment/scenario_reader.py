import json
import os
from pprint import pprint

import pandas as pd

from src.component.network import Network
from src.component.technique import Technique
from src.component.mitigation import Mitigation

class ScenarioReader:

    def _check(self, filepath):
        if not os.path.isfile(filepath):
            raise Exception("File does not exist")

    def read_network(self, filepath:str) -> Network:
        self._check(filepath)
        json_file = open(filepath)
        data = json.load(json_file)
        network = Network()
        network.initialize(data).set_foothold('External', 'Attacker')
        return network
    
    def read_technique_info(self, filepath:str) -> dict:
        self._check(filepath)
        technique_info = dict()
        
        df = pd.read_csv(filepath)
        for index, row in df.iterrows():
            technique_info[row['id']] = Technique(row['id'], 
                                                  row['complexity'], 
                                                  row['detection'], 
                                                  row['timestep'])
        return technique_info
    
    def read_mitigation_info(self, filepath:str) -> dict:
        self._check(filepath)
        mitigation_info = dict()
        
        df = pd.read_csv(filepath)
        for index, row in df.iterrows():
            mitigation_info[row['id']] = Mitigation(row['id'], 
                                                  row['complexity'], 
                                                  row['timestep'],
                                                  row['techniques'].split(','))
        return mitigation_info

    

if __name__ == '__main__':
    reader = ScenarioReader()
    network = reader.read_network("data/test_scenario.json")
    # print(network.subnets)
    reader.note_to_vec_host(network, render=True)

    # technique_info = reader.read_technique_info("data/technique_info.csv")
    # pprint(technique_info)
    # mitigation_info = reader.read_mitigation_info("data/mitigation_info.csv")
    # pprint(mitigation_info)
    # pprint(mitigation_info['M1017'].techniques)