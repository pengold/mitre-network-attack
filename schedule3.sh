# echo "Hello there!"
# echo "10"
# python3 -W ignore -m src.model.dqn -a dqn -s 10_2_5_normal -e 1000 -d easy #DQN
# python3 -W ignore -m src.model.dqn -a ddqn -s 10_2_5_normal -e 1000 -d easy #DDQN
# python3 -W ignore -m src.model.dqn -a dueling_dqn -s 10_2_5_normal -e 1000 -d easy #Dueling DQN
# python3 -W ignore -m src.model.actor_critic -t 0 -s 10_2_5_normal -e 1000 -d easy -k 0.01 #A2c
# python3 -W ignore -m src.model.actor_critic -s 10_2_5_normal -e 1000 -d easy -k 0.01 #WA 0.01
# python3 -W ignore -m src.model.actor_critic -s 10_2_5_normal -e 1000 -d easy -k 0.05 #WA 0.05
# python3 -W ignore -m src.model.actor_critic -s 10_2_5_normal -e 1000 -d easy -k 0.1 #WA 0.1

# echo "20"
# python3 -W ignore -m src.model.dqn -a dqn -s 20_2_5_normal -e 1000 -d easy #DQN
# python3 -W ignore -m src.model.dqn -a ddqn -s 20_2_5_normal -e 1000 -d easy #DDQN
# python3 -W ignore -m src.model.dqn -a dueling_dqn -s 20_2_5_normal -e 1000 -d easy #Dueling DQN
# python3 -W ignore -m src.model.actor_critic -t 0 -s 20_2_5_normal -e 1000 -d easy -k 0.01 #A2c
# python3 -W ignore -m src.model.actor_critic -s 20_2_5_normal -e 1000 -d easy -k 0.01 #WA 0.01
# python3 -W ignore -m src.model.actor_critic -s 20_2_5_normal -e 1000 -d easy -k 0.05 #WA 0.05
# python3 -W ignore -m src.model.actor_critic -s 20_2_5_normal -e 1000 -d easy -k 0.1 #WA 0.1

# echo "30"
# python3 -W ignore -m src.model.dqn -a dqn -s 30_3_5_normal -e 1000 -d easy #DQN
# python3 -W ignore -m src.model.dqn -a ddqn -s 30_3_5_normal -e 1000 -d easy #DDQN
# python3 -W ignore -m src.model.dqn -a dueling_dqn -s 30_3_5_normal -e 1000 -d easy #Dueling DQN
# python3 -W ignore -m src.model.actor_critic -t 0 -s 30_3_5_normal -e 1000 -d easy -k 0.01 #A2c
# python3 -W ignore -m src.model.actor_critic -s 30_3_5_normal -e 1000 -d easy -k 0.01 #WA 0.01
# python3 -W ignore -m src.model.actor_critic -s 30_3_5_normal -e 1000 -d easy -k 0.05 #WA 0.05
# python3 -W ignore -m src.model.actor_critic -s 30_3_5_normal -e 1000 -d easy -k 0.1 #WA 0.1

# echo "40"
# python3 -W ignore -m src.model.dqn -a dqn -s 40_4_5_normal -e 1000 -d easy #DQN
# python3 -W ignore -m src.model.dqn -a ddqn -s 40_4_5_normal -e 1000 -d easy #DDQN
# python3 -W ignore -m src.model.dqn -a dueling_dqn -s 40_4_5_normal -e 1000 -d easy #Dueling DQN
# python3 -W ignore -m src.model.actor_critic -t 0 -s 40_4_5_normal -e 1000 -d easy -k 0.01 #A2c
# python3 -W ignore -m src.model.actor_critic -s 40_4_5_normal -e 1000 -d easy -k 0.01 #WA 0.01
# python3 -W ignore -m src.model.actor_critic -s 40_4_5_normal -e 1000 -d easy -k 0.05 #WA 0.05
# python3 -W ignore -m src.model.actor_critic -s 40_4_5_normal -e 1000 -d easy -k 0.1 #WA 0.1

# echo "50"
# python3 -W ignore -m src.model.dqn -a dqn -s 50_5_5_normal -e 1000 -d easy #DQN
# python3 -W ignore -m src.model.dqn -a ddqn -s 50_5_5_normal -e 1000 -d easy #DDQN
# python3 -W ignore -m src.model.dqn -a dueling_dqn -s 50_5_5_normal -e 1000 -d easy #Dueling DQN
# python3 -W ignore -m src.model.actor_critic -t 0 -s 50_5_5_normal -e 1000 -d easy -k 0.01 #A2c
# python3 -W ignore -m src.model.actor_critic -s 50_5_5_normal -e 1000 -d easy -k 0.01 #WA 0.01
# python3 -W ignore -m src.model.actor_critic -s 50_5_5_normal -e 1000 -d easy -k 0.05 #WA 0.05
# python3 -W ignore -m src.model.actor_critic -s 50_5_5_normal -e 1000 -d easy -k 0.1 #WA 0.1

# echo "60"
# python3 -W ignore -m src.model.dqn -a dqn -s 60_6_5_normal -e 1000 -d easy #DQN
# python3 -W ignore -m src.model.dqn -a ddqn -s 60_6_5_normal -e 1000 -d easy #DDQN
# python3 -W ignore -m src.model.dqn -a dueling_dqn -s 60_6_5_normal -e 1000 -d easy #Dueling DQN
# python3 -W ignore -m src.model.actor_critic -t 0 -s 60_6_5_normal -e 1000 -d easy -k 0.01 #A2c
# python3 -W ignore -m src.model.actor_critic -s 60_6_5_normal -e 1000 -d easy -k 0.01 #WA 0.01
# python3 -W ignore -m src.model.actor_critic -s 60_6_5_normal -e 1000 -d easy -k 0.05 #WA 0.05
# python3 -W ignore -m src.model.actor_critic -s 60_6_5_normal -e 1000 -d easy -k 0.1 #WA 0.1

# echo "100"
# python3 -W ignore -m src.model.dqn -a dqn -s 100_10_5_normal -e 1000 -d easy #DQN
# python3 -W ignore -m src.model.dqn -a ddqn -s 100_10_5_normal -e 1000 -d easy #DDQN
# python3 -W ignore -m src.model.dqn -a dueling_dqn -s 100_10_5_normal -e 1000 -d easy #Dueling DQN
# python3 -W ignore -m src.model.actor_critic -t 0 -s 100_10_5_normal -e 1000 -d easy -k 0.01 #A2c
# python3 -W ignore -m src.model.actor_critic -s 100_10_5_normal -e 1000 -d easy -k 0.01 #WA 0.01
# python3 -W ignore -m src.model.actor_critic -s 100_10_5_normal -e 1000 -d easy -k 0.05 #WA 0.05
# python3 -W ignore -m src.model.actor_critic -s 100_10_5_normal -e 1000 -d easy -k 0.1 #WA 0.1

# echo "150"
# python3 -W ignore -m src.model.dqn -a dqn -s 150_15_5_normal -e 1000 -d easy #DQN
# python3 -W ignore -m src.model.dqn -a ddqn -s 150_15_5_normal -e 1000 -d easy #DDQN
# python3 -W ignore -m src.model.dqn -a dueling_dqn -s 150_15_5_normal -e 1000 -d easy #Dueling DQN
# python3 -W ignore -m src.model.actor_critic -t 0 -s 150_15_5_normal -e 1000 -d easy -k 0.01 #A2c
# python3 -W ignore -m src.model.actor_critic -s 150_15_5_normal -e 1000 -d easy -k 0.01 #WA 0.01
python3 -W ignore -m src.model.actor_critic -s 150_15_5_normal -e 1000 -d easy -k 0.05 #WA 0.05
python3 -W ignore -m src.model.actor_critic -s 150_15_5_normal -e 1000 -d easy -k 0.1 #WA 0.1