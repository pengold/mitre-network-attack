from __future__ import annotations
import json
from . import *
import pandas as pd

class Network:
    
    def __init__(self):
        self.subnets = dict()
        self.avail_techniques = list()
        
    def initialize(self, data) -> Network:
        # Read subnets
        for subnet_data in data['subnets']:
            subnet = Subnet(code=subnet_data['code'])
            self.subnets[subnet.code] = subnet

        # Read connections
        for connection_data in data['connections']:
            subnet_1 = self.subnets[connection_data[0]]
            subnet_2 = self.subnets[connection_data[1]]
            connection_forward = Connection(src=subnet_1, des=subnet_2)
            connection_backward = Connection(src=subnet_2, des=subnet_1)
            
            # Add connection to subnet
            for subnet in self.subnets.values():
                if subnet == connection_forward.src:
                    subnet.add_connection(connection_forward)
                if subnet == connection_backward.src:
                    subnet.add_connection(connection_backward)

        # Read hosts
        list_hosts = []
        dict_host_subnet = dict()
        for host_data in data['hosts']:
            host = Host.from_data(host_data)
            dict_host_subnet[host.code] = host_data['subnet']
            list_hosts.append(host)

        # Distribute hosts data to correlative subnet
        for subnet in self.subnets.values():
            for host in list_hosts:
                if dict_host_subnet[host.code] == subnet.code:
                    subnet.add_host(host)
                    host.subnet = subnet
        
        # Get availabe techniques
        for subnet in self.subnets.values():
            self.avail_techniques.extend(subnet.avail_techniques)
        self.avail_techniques.extend(data['additional_techniques'])
        self.avail_techniques = sorted(list(set(self.avail_techniques)))
        
        return self
    
    def set_foothold(self, subnet_code:str, host_code:str) -> Network:
        self.subnets[subnet_code].set_foothold(host_code)
        return self

    def set_subnet_vector(self, node2vec_model) -> Network:
        for subnet in self.subnets.values():
            subnet.vector = node2vec_model.wv[subnet.code]
        return self

                    
if __name__ == '__main__':
    json_file = open('data/test_scenario.json')
    data = json.load(json_file)
    df = pd.read_csv('data/technique_info.csv')
    for index, row in df.iterrows():
        print(row['id'], row['complexity'])
    
    net = Network()
    net.initialize(data).set_foothold('External', 'Attacker')
    print(net.subnets)
    print(net.subnets['S0'].hosts)
    
    print(net.subnets['S0'].hosts['M00'].avail_techniques)
    print(net.subnets['S0'].avail_techniques)
    print(net.avail_techniques)
    # print(net.subnets['S1'].hosts)
    # print(net.subnets['S2'].hosts)
    # print(net.avail_techniques)