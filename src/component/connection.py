class Connection:

    def __init__(self, src, des):
        self.src = src
        self.des = des

    def __repr__(self):
        return str(self.src) + " - " + str(self.des)
    