
import json
import copy
import random
import pickle
from pprint import pprint


from sklearn.neighbors import KNeighborsClassifier

from src.component.network import Network
from src.environment.action import Action, ActionSpace, ActionSpaceVector
from src.environment.knn import FaissKNeighbors
from src.environment.node2vec import TopologyEmbedding
from src.environment.reward import Reward
from src.environment.state import State


class MitreEnv:
    
    def __init__(self, network, technique_info=None, defender=None) -> None:
        self.network_copy = copy.deepcopy(network)
        self.network = network
        self.action_space = ActionSpace(self.network)
        self.state = State()
        
        self.technique_info = technique_info
        self.defender = defender
        
        print('Environment Information:')
        print('\tType: normal')
        print('\tAction space:', self.action_space.n)
    
    def _get_obs(self):
        return self.state.get_observation(self.network)
    
    def render(self) -> str:
        for subnet in self.network.subnets.values():
            print(subnet)
            for host in subnet.hosts.values():
                print(host.get_info())
                
    def reset(self):
        self.network = copy.deepcopy(self.network_copy)
        self.action_space = ActionSpace(self.network)
        self.state = State()
        if self.defender is not None:
            self.defender.network = self.network
        return self._get_obs()
        
    def step(self, action:int, state_changed:bool=True):      
        action = self.action_space.get_action(action)
        success = False
        reward = Reward.INIT
        done = False
        info = {'exploit_sensitive': False, 'success_defender': False}
        
        action_target = action.target if state_changed else copy.deepcopy(action.target)
        if action.type == Action.TYPE_SCAN:
            success = action_target.perform_scan()
            reward = Reward.SCAN if success else reward
        elif action.type == Action.TYPE_TECH:
            target_compromised = action_target.compromised

            # Add suspection for host (regardless it is perfomed successfully or not)
            action_target.add_suspection(self.technique_info[action.technique].detection)
            
            # Just apply for the host whose subnet is scanned
            if action_target.subnet.scanned:
            
                # Perform a technique based on its complexity
                success = False
                if random.uniform(0, 1) > self.technique_info[action.technique].complexity:
                    success = action_target.perform_technique(action.technique) 

                if success and not target_compromised:
                    reward = Reward.TECHNIQUE
                    if action_target.compromised:
                        # The host is compromised by perform the action
                        reward = Reward.COMPROMISED
                        if action_target.sensitive:
                            # If the target is sensitive host
                            reward = Reward.SENSITIVE
                            info['exploit_sensitive'] = True
                            # done = True
        
        done = True
        for subnet in self.network.subnets.values():
            for host in subnet.hosts.values():
                if host.sensitive and not host.compromised:
                    done = False
                    break
        
        if self.defender is not None:
            defender_info = self.defender.step()
            info['success_defender'] = defender_info['success_defender']
            if info['success_defender']:
                reward += Reward.SUCCESS_DEFENDER
                
        cur_state = self._get_obs()
        
        return cur_state, reward, done, info
        

class MitreEnvVector(MitreEnv):

    def __init__(self, network, defender=None, technique_info=None, tatic_emb_path=None, technique_emb_path=None, knn_rate=0.01) -> None:
        self.network_copy = copy.deepcopy(network)
        self.network = network
        self.state = State()
        
        self.technique_info = technique_info
        self.defender = defender

        self.tatic_emb = pickle.load(open(tatic_emb_path, 'rb'))
        self.technique_emb = pickle.load(open(technique_emb_path, 'rb'))
        self.topology_emb = TopologyEmbedding(dimensions=3).host_level(self.network)
        
        self.action_space = ActionSpaceVector(self.network, self.tatic_emb, self.topology_emb, self.technique_emb)
        
        self.knn = FaissKNeighbors(k=max(2, round(knn_rate * self.action_space.n)))
        self.knn.fit(self.action_space.space_vector, y=range(self.action_space.n))
        
        print("*" * 10, "ENVIRONMENT VECTOR", "*" * 10)
        print("Action space size:", self.action_space.n)
        print("Number of KNN neighbors:", max(2, round(knn_rate * self.action_space.n)))

    def reset(self):
        self.network = copy.deepcopy(self.network_copy)
        self.action_space = ActionSpaceVector(self.network, self.tatic_emb, self.topology_emb, self.technique_emb)
        self.state = State()
        if self.defender is not None:
            self.defender.network = self.network
        return self._get_obs()
    
    def get_knn_actions(self, proto_action):
        _, neigh_idx = self.knn.kneighbors([self.action_space.space_vector[proto_action]])
        return neigh_idx[0]