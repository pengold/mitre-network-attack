import random


class DefenderAgent:

    def __init__(self, network, mitigation_info, suspection_thres=0.2):
        self.network = network
        self.mitigation_info = mitigation_info
        self.suspection_thres = suspection_thres

    def step(self):
        info = {'success_defender': False}
        
        suspected_hosts = list()
        for subnet in self.network.subnets.values():
            for host in subnet.hosts.values():
                if host.suspection > self.suspection_thres:
                    suspected_hosts.append(host)

        if len(suspected_hosts) == 0:
            return info
        
        random_host = random.choice(suspected_hosts)
        random_mitigation_key = random.choice(
            list(self.mitigation_info.keys()))
        random_mitigation = self.mitigation_info[random_mitigation_key]
        random_success_rate = random.uniform(0, 1)

        if any(x for x in random_mitigation.techniques if x in random_host.avail_techniques) \
                and (random_success_rate > self.mitigation_info[random_mitigation_key].complexity):
            random_host.protected_by_defender()
            info['success_defender'] = True
        
        return info
